import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

//models
import { Device } from '../../models/Device';


//service
import { DeviceService } from '../../services/device.service';
import { TitleService } from '../../services/title.service';


@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.css']
})
export class DeviceListComponent implements OnInit {

  devices;
  isLoading: boolean = true;

  constructor(
    private _deviceService: DeviceService,
    private _titleService: TitleService,
  	private _toastr: ToastrService
  ) { }

  ngOnInit() {

    let loggedUserType: string = localStorage.getItem('authUserType');
    let loggedUserId: string = localStorage.getItem('userToken');

    this.sendNavTitle();

    if(loggedUserType == 'dealer'){
      //get only devices of the logged dealer
      
      this._deviceService.getDevicesByUserId(loggedUserId).subscribe((devices) => {
        this.isLoading = false;
        this.devices = devices.objects;
      })
    } else {
      this._deviceService.getDevices().subscribe((devices) => {
        this.isLoading = false;
        this.devices = devices.objects;
      })
    }
  	
  }

  sendNavTitle(): void {
      this._titleService.sendNavTitle('List of Device');
  }

  deleteDevice(deviceid, userid){
    this._toastr.error('<span class="now-ui-icons travel_info"></span> Something went wrong.', '', {
       timeOut: 5000,
       enableHtml: true,
       toastClass: "alert alert-danger alert-with-icon",
       positionClass: 'toast-top-center'
    });
    // console.log(deviceid, userid);
    // if(confirm('Are you sure?')){
    //   this._deviceService.deleteDevice(deviceid, userid).subscribe(() => {

    //     this.devices.forEach((curr, index) => {
    //       if(deviceid === curr.id){
    //         this.devices.splice(index, 1);
    //       }
    //     })
    //     this._toastr.error('<span class="now-ui-icons travel_info"></span> Dealer has been deleted.', '', {
    //        timeOut: 5000,
    //        enableHtml: true,
    //        toastClass: "alert alert-success alert-with-icon",
    //        positionClass: 'toast-top-center'
    //      });
    //   })  
    // }
  }

}
