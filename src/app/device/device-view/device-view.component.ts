import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

//models
import { User } from '../../models/User';

// services
import { UserService } from '../../services/user.service';
import { TitleService } from '../../services/title.service';
import { DeviceService } from '../../services/device.service';

@Component({
  selector: 'app-device-view',
  templateUrl: './device-view.component.html',
  styleUrls: ['./device-view.component.css']
})
export class DeviceViewComponent implements OnInit {

  isLoading: boolean = true;


  constructor(
  	private _toastr: ToastrService,
    private _userService: UserService,
    private _titleService: TitleService,
    private _deviceService: DeviceService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _location: Location
  ) { }

  ngOnInit() {
  	this.sendNavTitle();

  	const id = +this._route.snapshot.paramMap.get('id');



  }


  backButton(){
    this._location.back();
  }

  sendNavTitle(): void {
      this._titleService.sendNavTitle('Device Details');
  }

}
