import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

//models
import { User } from '../../models/User';
import { Device } from '../../models/Device';

// services
import { UserService } from '../../services/user.service';
import { DeviceService } from '../../services/device.service';
import { TitleService } from '../../services/title.service';


declare var $: any;
declare var jQuery: any;


@Component({
  selector: 'app-device-form',
  templateUrl: './device-form.component.html',
  styleUrls: ['./device-form.component.css']
})
export class DeviceFormComponent implements OnInit {

  users;
  auth_user_type_id: number;//for dealers

  device: Device = {
    device_name: '',
    device_id: '',
    device_longitude: '',
    devie_latitude: '',
    user_id: ''
  }

  onBtnLoading: boolean = false;

  constructor(
  	private _userService: UserService,
  	private _deviceService: DeviceService,
  	private toastr: ToastrService,
    private _titleService: TitleService,
  	private _router: Router
  ) { }

  ngOnInit() {

    this.sendNavTitle();

  	this.auth_user_type_id = 3;
  	this._userService.getUsersOfType(this.auth_user_type_id).subscribe((users) => {
  		this.users = users.objects;
  	})

  }

  sendNavTitle(): void {
      this._titleService.sendNavTitle('Add Device');
  }

  onSubmit({value, valid}: {value: any, valid: boolean}){
  	if(!valid){
  	  this.toastr.error('<span class="now-ui-icons travel_info"></span> Please enter all details correctly', '', {
         timeOut: 5000,
         enableHtml: true,
         toastClass: "alert alert-danger alert-with-icon",
         positionClass: 'toast-top-center'
      });
  	} else {
      this.onBtnLoading = true;
  		this._deviceService.addDevice(value).subscribe(device => {
  			this._router.navigate(['/device']);
	        this.toastr.error('<span class="now-ui-icons travel_info"></span> Device has been added.', '', {
	          timeOut: 5000,
	          enableHtml: true,
	          toastClass: "alert alert-success alert-with-icon",
	          positionClass: 'toast-top-center'
	        });
  		})
  	}
  }

}
