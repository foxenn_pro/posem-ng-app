import { Component, OnInit } from '@angular/core';

//models
import { User } from '../../models/User';

//services
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  user: User = {
  	email: ''
  }

  constructor() { }

  ngOnInit() {
  }

  onSubmit(emailid){
  	console.log(emailid);
  }

}
