import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Http, Response } from '@angular/http';
import { ToastrService } from 'ngx-toastr';

//service
import { AuthService } from '../../services/auth.service';

//models
import { User } from '../../models/User';

//services
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User = {
  	email: '',
  	password: ''
  }

  isLoginErr: boolean = false;

  onBtnLoading: boolean = false;

  constructor(
    private userService: UserService,
    private router: Router,
    private toastr: ToastrService,
    private auth: AuthService
   ) { }



  ngOnInit() {
    //check loggedin
    if(this.auth.isAuthenticated()){
      this.router.navigate(['/dashboard'])
    }
  }

  //login submit
  onSubmit(email, password){
    if(!email || !password){
      this.toastr.error('<span class="now-ui-icons travel_info"></span> Please enter all details', '', {
         timeOut: 5000,
         enableHtml: true,
         toastClass: "alert alert-danger alert-with-icon",
         positionClass: 'toast-top-center'
      });
    } else {
      this.onBtnLoading = true;
      this.userService.userAuthentication({email, password} as User).subscribe(( user: any) => {
        if(user.id !== null){
          //store cred
          localStorage.setItem('userToken', user.id);
          localStorage.setItem('userName', user.firstName);
          localStorage.setItem('authUserType', user.auth_user_type);
          this.router.navigate(['/dashboard']);
          this.toastr.error('<span class="now-ui-icons travel_info"></span> Login Success', '', {
           timeOut: 5000,
           enableHtml: true,
           toastClass: "alert alert-success alert-with-icon",
           positionClass: 'toast-top-center'
         });
        }
      }, (err) => {
        this.onBtnLoading = false;
        this.toastr.error('<span class="now-ui-icons travel_info"></span> Invalid Email ID or Password', '', {
           timeOut: 5000,
           enableHtml: true,
           toastClass: "alert alert-danger alert-with-icon",
           positionClass: 'toast-top-center'
         });
      });
    }
  }

}
