import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';

import { DealerListComponent } from '../../user/dealer-list/dealer-list.component';
import { DealerViewComponent } from '../../user/dealer-view/dealer-view.component';
import { DealerFormComponent } from '../../user/dealer-form/dealer-form.component';

import { DealerAdminFormComponent } from '../../user/dealer-admin-form/dealer-admin-form.component';
import { DealerAdminViewComponent } from '../../user/dealer-admin-view/dealer-admin-view.component';
import { DealerAdminListComponent } from '../../user/dealer-admin-list/dealer-admin-list.component';

import { DeviceListComponent } from '../../device/device-list/device-list.component';
import { DeviceViewComponent } from '../../device/device-view/device-view.component';
import { DeviceFormComponent } from '../../device/device-form/device-form.component';

import { RoleGuardService as RoleGuard } from '../../services/role-guard.service';

export const AdminLayoutRoutes: Routes = [
	{ 
		path: 'dashboard',      
    	component: DashboardComponent ,
    	canActivate: [RoleGuard],
    	data: { 
			expectedRole: ['dealer', 'admin' , 'dealer-admin']
		}
	},
	{ 
		path: 'dealer-admin',      
    	component: DealerAdminListComponent ,
    	canActivate: [RoleGuard],
    	data: { 
			expectedRole: ['admin']
		}
	},
	{ 
		path: 'dealer-admin/add',      
    	component: DealerAdminFormComponent ,
    	canActivate: [RoleGuard],
    	data: { 
			expectedRole: ['admin']
		}
	},
	{ 
		path: 'dealer-admin/view/:id',      
    	component: DealerAdminViewComponent ,
    	canActivate: [RoleGuard],
    	data: { 
			expectedRole: ['admin']
		}
	},
	{ 
		path: 'dealer-admin/edit/:id',      
    	component: DealerAdminFormComponent ,
    	canActivate: [RoleGuard],
    	data: { 
			expectedRole: ['admin']
		}
	},
	{ 
		path: 'dealer',      
    	component: DealerListComponent ,
    	canActivate: [RoleGuard],
    	data: { 
			expectedRole: ['admin', 'dealer-admin']
		}
	},
	{ 
		path: 'dealer/add',      
    	component: DealerFormComponent ,
    	canActivate: [RoleGuard],
    	data: { 
			expectedRole: ['dealer-admin']
		}
	},
	{ 
		path: 'dealer/edit/:id',      
    	component: DealerFormComponent ,
    	canActivate: [RoleGuard],
    	data: { 
			expectedRole: ['dealer-admin']
		}
	},
	{ 
		path: 'dealer/view/:id',      
    	component: DealerViewComponent ,
    	canActivate: [RoleGuard],
    	data: { 
			expectedRole: ['dealer-admin']
		}
	},
	{ 
		path: 'device',      
    	component: DeviceListComponent ,
    	canActivate: [RoleGuard],
    	data: { 
			expectedRole: ['dealer', 'admin', 'dealer-admin']
		}
	},
	{ 
		path: 'device/add',      
    	component: DeviceFormComponent ,
    	canActivate: [RoleGuard],
    	data: { 
			expectedRole: ['admin', 'dealer-admin']
		}
	},
	{ 
		path: 'device/view/:id',      
    	component: DeviceViewComponent ,
    	canActivate: [RoleGuard],
    	data: { 
			expectedRole: ['admin', 'dealer-admin']
		}
	},
	{ 
		path: 'user-profile',   
		component: UserProfileComponent 
	},
	{ 
		path: 'table-list',     
		component: TableListComponent 
	},
	{ 
		path: 'typography',     
		component: TypographyComponent 
	},
	{ 
		path: 'icons',          
		component: IconsComponent 
	},
	{ 
    	path: 'maps',           
    	component: MapsComponent 
    },
    { 
    	path: 'notifications',  
    	component: NotificationsComponent 
    }
];
