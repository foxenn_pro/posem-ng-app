import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';

import { DealerListComponent } from '../../user/dealer-list/dealer-list.component';
import { DealerViewComponent } from '../../user/dealer-view/dealer-view.component';
import { DealerFormComponent } from '../../user/dealer-form/dealer-form.component';

import { DealerAdminFormComponent } from '../../user/dealer-admin-form/dealer-admin-form.component';
import { DealerAdminListComponent } from '../../user/dealer-admin-list/dealer-admin-list.component';
import { DealerAdminViewComponent } from '../../user/dealer-admin-view/dealer-admin-view.component';

import { DeviceListComponent } from '../../device/device-list/device-list.component';
import { DeviceViewComponent } from '../../device/device-view/device-view.component';
import { DeviceFormComponent } from '../../device/device-form/device-form.component';

import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

//pipes
import { SortGridPipe } from '../../pipes/sortgrid.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ChartsModule,
    NgbModule
  ],
  declarations: [
    DashboardComponent,
    DealerListComponent,
    DealerFormComponent,
    DealerAdminFormComponent,
    DealerAdminListComponent,
    DeviceListComponent,
    DeviceFormComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    DealerAdminViewComponent,
    DealerViewComponent,
    DeviceViewComponent,
    SortGridPipe
  ]
})

export class AdminLayoutModule {}
