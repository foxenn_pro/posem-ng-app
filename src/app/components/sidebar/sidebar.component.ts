import { Component, OnInit, EventEmitter, Output } from '@angular/core';

import { User } from '../../models/User';

//service import
import { UserService } from '../../services/user.service';



declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    rolePermit?: string;
}

export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'design_app', class: '' },
    { path: '/dealer-admin', title: 'Dealer Admin',  icon: 'users_single-02', class: '' },
    { path: '/dealer', title: 'Dealer',  icon: 'users_single-02', class: '' },
    { path: '/icons', title: 'Icons',  icon:'education_atom', class: '' },
    { path: '/maps', title: 'Maps',  icon:'location_map-big', class: '' },
    { path: '/notifications', title: 'Notifications',  icon:'ui-1_bell-53', class: '' },

    { path: '/user-profile', title: 'User Profile',  icon:'users_single-02', class: '' },
    { path: '/table-list', title: 'Table List',  icon:'design_bullet-list-67', class: '' },
    { path: '/typography', title: 'Typography',  icon:'text_caps-small', class: '' }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  Role: string;

  constructor(
    private _userService: UserService
  ) { 

    //disable data according to roles
    const authUserType = localStorage.getItem('authUserType');
    this.Role = authUserType;
  }

  roleNotDealer(){
    if(this.Role == 'dealer'){
      return false;
    } else {
      return true;
    }
  }

  roleNotAdmin(){
    if(this.Role == 'admin'){
      return false;
    } else {
      return true;
    }
  }
  roleNotDealerAdmin(){
    if(this.Role == 'dealer-admin'){
      return false;
    } else {
      return true;
    }
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ( window.innerWidth > 991) {
          return false;
      }
      return true;
  };


}
