export interface User{
	app_id?: string,
	auth_user_type?: {
		app_id?: string,
		auth_user_name?: string,
		auth_user_type?: string,
		date_created?: string,
		date_last_modified?: string,
		device_id?: string,
		entity_state?: string,
		id?: number,
		resource_uri?: string
	},
	date_created?: string,
	date_last_modified?: string,
	designation?: string
	device_id?: string,
	email?: string,
	entity_id?: string
	entity_state?: string,
	firstName?: string,
	id?: string,
	image_url?: string,
	is_active?: boolean,
	is_admin?: boolean,
	is_staff?: boolean,
	is_superuser?: boolean,
	lastName?: string,
	last_login?: string,
	new_member?: boolean,
	owner_id?: number,
	password?: string,
	resource_uri?: string,
	user_addressline1?: string,
	user_addressline2?: string,
	user_city?: string,
	user_color?: string,
	user_country?: string,
	user_date_of_birth?: string,
	user_gender?: string,
	user_image?: string,
	user_mobile_number?: string,
	user_organization?: string,
	user_state?: string,
	user_street_name?: string,
	user_street_number?: string,
	user_zip_code?: string
 }



