export interface Device {
  app_id?: string,
  date_created?: string,
  date_last_modified?: string,
  device_id?: string,
  device_longitude?: string,
  device_name?: string,
  devie_latitude?: string,
  entity_state?: number,
  id?: number,
  resource_uri?: number,
  user_id?: string
}