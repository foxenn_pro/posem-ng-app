import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  constructor() { }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('userToken');
    if (!token) {
      // console.log("Token does not exists");
      return false;
    }
    else {
      // console.log("Token exists");
      return true;
    }
  }

}
