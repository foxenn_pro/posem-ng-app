import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

//models
import { Device } from '../models/Device';
import { User } from '../models/User';

//set headers
const httpOptions = {
	headers: new HttpHeaders({
		'Content-Type':'application/json', 
		'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
    'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
	})
}

@Injectable()
export class DeviceService {

  readonly rootUrl: string = 'http://posem-dev-1001.appspot.com/';
  readonly appIdUrl: string = '/?app_id=c3a15eed228552dd85d5';

  deleteData;

  constructor(private _http: HttpClient) { }

  getDevicesUrl: string = 'iot/service/v1/device'
  getDevices(): Observable<any>{
  	return this._http.get<any>(this.rootUrl + this.getDevicesUrl + this.appIdUrl);
  }

  addDeviceUrl: string = 'iot/service/v1/adddevice'
  addDevice(value): Observable<any>{
  	return this._http.post<any>(this.rootUrl + this.addDeviceUrl + this.appIdUrl, value, httpOptions);
  }

  deleteDeviceUrl: string = 'iot/service/v1/deletedevice'
  deleteDevice(deviceid, userid){
    this.deleteData = {
      device_id: deviceid,
      user_id: userid
    }
    return this._http.post<any>(this.rootUrl + this.deleteDeviceUrl + this.appIdUrl, this.deleteData, httpOptions);
  }

  getDevicesByUserIdUrl: string = 'iot/service/v1/device/?app_id=c3a15eed228552dd85d5&user_id=1 ';
  getDevicesByUserId(id): Observable<any>{
    return this._http.get<any>(this.rootUrl + this.getDevicesByUserIdUrl + this.appIdUrl + '&user_id=' + id);
  }

}
