import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class TitleService {

  private subject = new Subject<any>();

  constructor() { }

  sendNavTitle(title: string){
  	this.subject.next({ text: title });
  }

  getNavTitle(){
  	return this.subject.asObservable();
  }

}
