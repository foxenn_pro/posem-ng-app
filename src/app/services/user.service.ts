import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

//model
import { User } from '../models/User';

//set headers
const httpOptions = {
	headers: new HttpHeaders({
		'Content-Type':'application/json', 
		'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
    'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
	})
}

@Injectable()
export class UserService {

  readonly rootUrl: string = 'http://posem-dev-1001.appspot.com/';
  readonly appIdUrl: string = '/?app_id=c3a15eed228552dd85d5';

  constructor(private _http: HttpClient) {  }

  //login post
  loginUrl: string = 'auth/service/v1/user/login' + this.appIdUrl;
  userAuthentication(userCred: User): Observable<User>{
  	return this._http.post<User>(this.rootUrl + this.loginUrl, userCred , httpOptions);
  }

  //get user[] of type
  getUsersOfTypeUrl: string = 'auth/service/v1/register' + this.appIdUrl + '&auth_user_type=';
  getUsersOfType(auth_user_id): Observable<any>{
    return this._http.get<any>(this.rootUrl + this.getUsersOfTypeUrl + auth_user_id);
  }

  //add dealer admin & dealer
  addUserUrl: string = 'auth/service/v1/register' + this.appIdUrl;
  addUser(value: any): Observable<any>{
    return this._http.post<any>(this.rootUrl + this.addUserUrl, value , httpOptions);
  }

  getUserByIdUrl: string = 'auth/service/v1/register/';
  getUserById(id: any): Observable<User>{
    return this._http.get<User>(this.rootUrl + this.getUserByIdUrl + id + this.appIdUrl);
  }


  //delete User
  deleteUserByIdUrl: string = 'auth/service/v1/register/';
  deleteUserById(id: User): Observable<User>{
    return this._http.delete<User>(this.rootUrl + this.deleteUserByIdUrl + id + this.appIdUrl);
  }

  updateUserByIdUrl: string = 'auth/service/v1/register/';
  updateUserById(id: any, value): Observable<User>{
    return this._http.patch<User>(this.rootUrl + this.updateUserByIdUrl + id + this.appIdUrl, value, httpOptions)
  }

}




