import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class RoleGuardService implements CanActivate{

  constructor(public auth: AuthService, public router: Router, private toastr: ToastrService) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {

	let expectedRoleArray = route.data;
	expectedRoleArray = expectedRoleArray.expectedRole;

	const userType = localStorage.getItem('authUserType');

	let  expectedRole = '';
 
    for(let i=0; i<expectedRoleArray.length; i++){
      if(expectedRoleArray[i]== userType){
        // console.log("Roles Matched");
        expectedRole = userType;
      }
    }

    if (this.auth.isAuthenticated() && userType == expectedRole) {
    	// console.log("User permitted");
    	return true;
    } else if(this.auth.isAuthenticated()){
      this.toastr.error('<span class="now-ui-icons travel_info"></span> Not Permitted Route', '', {
         timeOut: 5000,
         enableHtml: true,
         toastClass: "alert alert-danger alert-with-icon",
         positionClass: 'toast-top-center'
      });
    	this.router.navigate(['/dashboard']);
    	return false;
    }

    this.router.navigate(['/login']);
    return false;
    
  }

}
