import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

//models
import { User } from '../../models/User';

// services
import { UserService } from '../../services/user.service';
import { TitleService } from '../../services/title.service';

@Component({
  selector: 'app-dealer-admin-view',
  templateUrl: './dealer-admin-view.component.html',
  styleUrls: ['./dealer-admin-view.component.css']
})
export class DealerAdminViewComponent implements OnInit {

  user: User;

  isLoading: boolean = true;


  constructor(
  	private _toastr: ToastrService,
    private _userService: UserService,
    private _titleService: TitleService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _location: Location
  ) { }

  ngOnInit() {
  	this.sendNavTitle();


  	const id = +this._route.snapshot.paramMap.get('id');

  	this._userService.getUserById(id).subscribe(user => {
  		this.isLoading = false;
  		this.user = user;
  	})

  	
  }

  backButton(){
    this._location.back();
  }

  sendNavTitle(): void {
      this._titleService.sendNavTitle('Dealer Admin Profile');
  }

}
