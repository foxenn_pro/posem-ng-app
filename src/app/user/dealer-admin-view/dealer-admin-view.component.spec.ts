import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealerAdminViewComponent } from './dealer-admin-view.component';

describe('DealerAdminViewComponent', () => {
  let component: DealerAdminViewComponent;
  let fixture: ComponentFixture<DealerAdminViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealerAdminViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealerAdminViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
