import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealerAdminFormComponent } from './dealer-admin-form.component';

describe('DealerAdminFormComponent', () => {
  let component: DealerAdminFormComponent;
  let fixture: ComponentFixture<DealerAdminFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealerAdminFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealerAdminFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
