import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

//models
import { User } from '../../models/User';

// services
import { UserService } from '../../services/user.service';
import { TitleService } from '../../services/title.service';

@Component({
  selector: 'app-dealer-admin-form',
  templateUrl: './dealer-admin-form.component.html',
  styleUrls: ['./dealer-admin-form.component.css']
})
export class DealerAdminFormComponent implements OnInit {

  user: User = {
  	firstName: '',
  	lastName: '',
  	email: '',
  	user_city: '',
  	password: ''
  }

  isEditing: boolean = false;
  onBtnLoading: boolean = false;

  origPassword: string;

  constructor(
  	private toastr: ToastrService,
  	private _userService: UserService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _titleService: TitleService
  ) { }

  sendNavTitle(): void {
      this._titleService.sendNavTitle('Add Dealer Admin');
  }

  ngOnInit() {
    this.sendNavTitle();
    //check if id exist
    this.getUser();
  }

  getUser(){
    //for edit purpose
    const id = +this._route.snapshot.paramMap.get('id');
    if(id){
      this.isEditing = true;
      this._userService.getUserById(id).subscribe((user: User) => {
        this.user = user;
        this.origPassword = this.user.password;
      })
    }
  }


  onSubmit({value, valid}: { value: any, valid: boolean }){

    //for edit purpose
    const id = +this._route.snapshot.paramMap.get('id');

  	if(!valid){
  	  this.toastr.error('<span class="now-ui-icons travel_info"></span> Please enter all details correctly', '', {
         timeOut: 5000,
         enableHtml: true,
         toastClass: "alert alert-danger alert-with-icon",
         positionClass: 'toast-top-center'
      });
  	} else {

      this.onBtnLoading = true;

      //check if edit in params
      if(id){ 
        if(this.checkUserPasswordChange(value.password)){
          //password unchanged
          delete value.password;
          this.updateUserCall(value);
        } else {
          //password has been changed
          this.updateUserCall(value);
        }
      } else {
        this.addUserCall(value);
      }
  	}
  }

  checkUserPasswordChange(password){
    return (password === this.origPassword) ? true : false;
  }

  updateUserCall(value){
    this._userService.updateUserById(this.user.id, value).subscribe((user: User) => {
      this._router.navigate(['/dealer-admin']);
      this.toastr.error('<span class="now-ui-icons travel_info"></span> Dealer Admin has been updated.', '', {
        timeOut: 5000,
        enableHtml: true,
        toastClass: "alert alert-success alert-with-icon",
        positionClass: 'toast-top-center'
      });

    }, (err) => {
      this.onBtnLoading = false;
      this.toastr.error('<span class="now-ui-icons travel_info"></span> Something Wrong, Try Again', '', {
         timeOut: 5000,
         enableHtml: true,
         toastClass: "alert alert-danger alert-with-icon",
         positionClass: 'toast-top-center'
       });
    })
  }

  addUserCall(value){
    //set auth_user_type
    value.auth_user_type = '/auth/service/v1/usertype/2/'; //because its dealer admin
    this._userService.addUser(value).subscribe(user => {
      this._router.navigate(['/dealer-admin']);
      this.toastr.error('<span class="now-ui-icons travel_info"></span> Dealer Admin has been added.', '', {
         timeOut: 5000,
         enableHtml: true,
         toastClass: "alert alert-success alert-with-icon",
         positionClass: 'toast-top-center'
       });
    }, (err) => {
      this.onBtnLoading = false;
      this.toastr.error('<span class="now-ui-icons travel_info"></span> Something Wrong, Try Again', '', {
         timeOut: 5000,
         enableHtml: true,
         toastClass: "alert alert-danger alert-with-icon",
         positionClass: 'toast-top-center'
       });
    })
  }

}
