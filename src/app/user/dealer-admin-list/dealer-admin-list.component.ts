import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

//models
import { User } from '../../models/User';

// services
import { UserService } from '../../services/user.service';
import { TitleService } from '../../services/title.service';

@Component({
  selector: 'app-dealer-admin-list',
  templateUrl: './dealer-admin-list.component.html',
  styleUrls: ['./dealer-admin-list.component.css']
})

export class DealerAdminListComponent implements OnInit {

  users: User[];
  isLoading: boolean = true;
  //usertype for dealer-list is 2
  auth_user_type_id: number;

  @Output() editNewUser: EventEmitter<User> = new EventEmitter();

  constructor(
    private _toastr: ToastrService,
    private _userService: UserService,
    private _titleService: TitleService
   ) { }

  ngOnInit() {
    this.sendNavTitle();
    this.auth_user_type_id = 2
    this._userService.getUsersOfType(this.auth_user_type_id).subscribe((users) => {
      this.isLoading = false;
      this.users = users.objects;
    })
  }

  sendNavTitle(): void {
      this._titleService.sendNavTitle('List of Dealer Admin');
  }

  deleteUser(id: User){

    if(confirm('Are you sure?')){
      this._userService.deleteUserById(id).subscribe(() => {

        this.users.forEach((curr, index) => {
          if(id === curr.id){
            this.users.splice(index, 1);
          }
        })

        this._toastr.error('<span class="now-ui-icons travel_info"></span> Dealer admin has been deleted.', '', {
           timeOut: 5000,
           enableHtml: true,
           toastClass: "alert alert-success alert-with-icon",
           positionClass: 'toast-top-center'
         });
        
      })  

    }

  }

}
