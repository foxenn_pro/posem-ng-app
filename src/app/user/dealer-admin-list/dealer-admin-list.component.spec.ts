import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealerAdminListComponent } from './dealer-admin-list.component';

describe('DealerAdminListComponent', () => {
  let component: DealerAdminListComponent;
  let fixture: ComponentFixture<DealerAdminListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealerAdminListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealerAdminListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
