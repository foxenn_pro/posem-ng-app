import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

//models
import { User } from '../../models/User';
import { Device } from '../../models/Device';

// services
import { UserService } from '../../services/user.service';
import { TitleService } from '../../services/title.service';
import { DeviceService } from '../../services/device.service';

@Component({
  selector: 'app-dealer-view',
  templateUrl: './dealer-view.component.html',
  styleUrls: ['./dealer-view.component.css']
})
export class DealerViewComponent implements OnInit {

  user: User;

  devices: Device[];

  isLoading: boolean = true;

  constructor(
  	private _toastr: ToastrService,
    private _userService: UserService,
    private _titleService: TitleService,
    private _deviceService: DeviceService,
    private _route: ActivatedRoute,
    private _location: Location,
    private _router: Router
  ) { }

  ngOnInit() {
  	this.sendNavTitle();

  	const id = +this._route.snapshot.paramMap.get('id');

  	this._userService.getUserById(id).subscribe(user => {
  		this.isLoading = false;
  		this.user = user;
  	})

  	this._deviceService.getDevicesByUserId(id).subscribe((devices) => {
  		this.devices = devices.objects;
  	})
  }

  backButton(){
    this._location.back();
  }


  sendNavTitle(): void {
      this._titleService.sendNavTitle('Dealer Profile');
  }

}
