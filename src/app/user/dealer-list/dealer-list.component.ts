import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

//models
import { User } from '../../models/User';

// services
import { UserService } from '../../services/user.service';
import { TitleService } from '../../services/title.service';

@Component({
  selector: 'app-dealer-list',
  templateUrl: './dealer-list.component.html',
  styleUrls: ['./dealer-list.component.css']
})
export class DealerListComponent implements OnInit {

  users: User[];
  isLoading: boolean = true;
  Role: string;


  constructor(
    private _toastr: ToastrService,
    private _userService: UserService,
    private _titleService: TitleService

  ) { 
    //disable data according to roles
    const authUserType = localStorage.getItem('authUserType');
    this.Role = authUserType;
  }

  //usertype for dealer-list is 2
  auth_user_type_id: number;
  ngOnInit() {
    this.sendNavTitle();
    this.auth_user_type_id = 3;
  	this._userService.getUsersOfType(this.auth_user_type_id).subscribe((users) => {
      this.isLoading = false;
  		this.users = users.objects;
  	})
  }

  sendNavTitle(): void {
      this._titleService.sendNavTitle('List of Dealer');
  }

  deleteUser(id: User){
    if(confirm('Are you sure?')){
      this._userService.deleteUserById(id).subscribe(() => {
        this.users.forEach((curr, index) => {
          if(id === curr.id){
            this.users.splice(index, 1);
          }
        })
        this._toastr.error('<span class="now-ui-icons travel_info"></span> Dealer has been deleted.', '', {
           timeOut: 5000,
           enableHtml: true,
           toastClass: "alert alert-success alert-with-icon",
           positionClass: 'toast-top-center'
         });
      })  
    }
  }

  roleNotDealer(){
    if(this.Role == 'dealer'){
      return false;
    } else {
      return true;
    }
  }

  roleNotAdmin(){
    if(this.Role == 'admin'){
      return false;
    } else {
      return true;
    }
  }
  roleNotDealerAdmin(){
    if(this.Role == 'dealer-admin'){
      return false;
    } else {
      return true;
    }
  }
}
